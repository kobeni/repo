/* See LICENSE file for copyright and license details. */
#include <stdio.h>
#include <sys/sysinfo.h>

#include "util.h"

static void
usage(void)
{
	eprintf("usage: %s\n", argv0);
}

int
main(int argc, char *argv[])
{
	ARGBEGIN {
	default:
		usage();
	} ARGEND

	printf( "%d\n", get_nprocs() );
	return 0;
}
