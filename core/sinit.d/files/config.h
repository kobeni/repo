/* See LICENSE file for copyright and license details. */

#define LIBEXEC "/usr/libexec/init/"
static char *const rcinitcmd[]     = { LIBEXEC "boot", NULL };
static char *const rcrebootcmd[]   = { LIBEXEC "shut", "r", NULL };
static char *const rcpoweroffcmd[] = { LIBEXEC "shut", "p", NULL };
