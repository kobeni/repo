#!/bin/sh

# Protect /etc direcotry 

for file in `find /~etc/`
do
	test -d ${file} && mkdir /etc/${file#/~etc/} 2>/dev/null
	test -f ${file} -a ! -f /etc/${file#/~etc/} && mv $file /etc/${file#/~etc/} 
done

rm -r /~etc
