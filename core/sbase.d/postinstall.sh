#!/bin/sh

cd "$1/usr/bin"

# sbase-box safe install
ln -sf sbase-box-bin mv
rm sbase-box
mv sbase-box-bin sbase-box
ln -sf sbase-box mv
