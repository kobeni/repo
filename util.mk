DOWNLOAD = curl -sLO 
SHA256   = test `sha256sum $@ |sed 's/\s.*//'` == 
SHA256D  = test `find $@ -type f |xargs sha256sum|sort|sha256sum|sed 's/\s.*//'` ==

GITCLONE = \
	git clone $$url/$$pkg &&\
        git -C $$pkg reset --hard $$commit &&\
	rm -r $$pkg/.git
