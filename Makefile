.POSIX:

# VARIABLES
POSIX=yes
MAN=yes
CACHEDIR = ${XDG_CACHE_HOME}/bmpm
BUILDDIR = ${XDG_RUNTIME_DIR}/bmpm

# PROTOTYPES
.SUFFIXES: .d .pkg .src

.d.pkg:
	@mkdir -p ${BUILDDIR}
	@${MAKE} -C $< ${CACHEDIR}/`basename $@`
	@rm -rf ${BUILDDIR}

.d.src:
	@mkdir -p ${BUILDDIR}
	@${MAKE} -C $< ${CACHEDIR}/`basename $@`
	@rm -rf ${BUILDDIR}

.d:
	@mkdir -p ${BUILDDIR}
	@${MAKE} package=`basename $@` uninstall-${u}
	@rm -rf ${BUILDDIR}

# DEFAULT
list:
	@ls /var/db/bmpm/ | sed -n 's/.txt//p'

# BUILD RULES
${CACHEDIR}/${package}.pkg: ${CACHEDIR} ${BUILDDIR}/root
	@tar -zcf $@ -C ${BUILDDIR}/root .

${BUILDDIR}/root: ${BUILDDIR}/build
	@${MAKE} -C ${BUILDDIR}/build -f ${PWD}/Makefile root=$@ \
		root root-posix-${POSIX} root-man-${MAN}
	
	@test -f ${PWD}/postinstall.sh && install -Dm 755 ${PWD}/postinstall.sh "$@/var/db/bmpm/${package}.sh" || true

	@mkdir -p "$@/var/db/bmpm/"
	@find $@ |sed "s,$@/,," |grep -v ${BUILDDIR} |sort > "$@/var/db/bmpm/${package}.txt"

${BUILDDIR}/build: ${CACHEDIR}/${package}.src
	@mkdir -p $@
	@tar -zxf ${CACHEDIR}/${package}.src -C $@

${CACHEDIR}/${package}.src:
	@test -d files && mkdir -p files && cp -R files ${BUILDDIR}/files || true
	@${MAKE} -C ${BUILDDIR} -f ${PWD}/Makefile prepare
	@tar -zcf $@ -C ${BUILDDIR}/build .

${CACHEDIR}:
	@mkdir -p $@
	
# OPTION BUILDS
root-posix:
root-posix-no:
root-posix-yes: root-posix
root-man:
root-man-yes: root-man
root-man-no:

# INSTALL RULES
u=no
ALL_FILES = ls ${DESTDIR}/var/db/bmpm/*.txt|grep -v ${package}.txt |xargs sort -u
PKG_FILES = ${BUILDDIR}/${package}.txt
uninstall-no: install
uninstall-yes: uninstall

install: ${CACHEDIR}/${package}.src check-root
	@tar -ztf ${CACHEDIR}/${package}.pkg | sed 's,^\./\?,,' |sort > ${PKG_FILES}
	@test -f "${DESTDIR}/var/db/bmpm/${package}.txt"  && \
		${MAKE} loop=remove loop-no `comm -23 ${DESTDIR}/var/db/bmpm/${package}.txt ${PKG_FILES}` || \
		${MAKE} loop=check loop-no `${ALL_FILES}|comm -12 - ${PKG_FILES}`
	@mkdir -p ${BUILDDIR}
	@tar -zxf ${CACHEDIR}/${package}.pkg -C ${BUILDDIR} 
	@${MAKE} loop=check-package-lib loop-no `grep "^usr/bin/\|^usr/lib/\|^usr/libexec/" ${PKG_FILES}`
	@${MAKE} loop=install loop-no `${ALL_FILES}|comm -13 - ${PKG_FILES}`
	@chmod 644 "${DESTDIR}/var/db/bmpm/${package}.txt"
	@test -f "${DESTDIR}/var/db/bmpm/${package}.sh" && ${DESTDIR}/var/db/bmpm/${package}.sh ${DESTDIR} || true
	@rm -rf ${BUILDDIR}

uninstall: ${DESTDIR}/var/db/bmpm/${package}.txt check-root
	@test -f "${DESTDIR}/var/db/bmpm/${package}.txt" 
	@${MAKE} loop=remove `${ALL_FILES}|comm -13 - ${DESTDIR}/var/db/bmpm/${package}.txt`

check-root:
	@echo check if root
	@test `id -u` -eq 0

# LOOP RULES
loop=no

.DEFAULT:
	@${MAKE} file=$@ loop-${loop}

loop-no:

loop-check:
	@test -d ${DESTDIR}/${file} || echo "Checking file: /${file}"
	@test ! -f ${DESTDIR}/${file} 
	

loop-install:
	@echo "Installing: ${DESTDIR}/${file}"
	@test -f ${DESTDIR}/${file} && rm ${DESTDIR}/${file} || true
	@test -d ${BUILDDIR}/${file} && mkdir -p ${DESTDIR}/${file} || true
	@test -f ${DESTDIR}/${file} -a ! -L ${DESTDIR}/${file} && rm ${DESTDIR}/${file} || true
	@test -f ${BUILDDIR}/${file} -a ! -L ${BUILDDIR}/${file} && mv ${BUILDDIR}/${file} ${DESTDIR}/${file} || true
	@test -L ${BUILDDIR}/${file} && ln -sf `readlink ${BUILDDIR}/${file}` ${DESTDIR}/${file} || true
	@test ! -L ${BUILDDIR}/${file} && chown root:root ${DESTDIR}/${file} || true

loop-remove:
	@echo "Removing: ${DESTDIR}/${file}"
	@test -d ${DESTDIR}/${file} && rmdir ${DESTDIR}/${file}  || true
	@test -f ${DESTDIR}/${file} -a ! -L ${DESTDIR}/${file} && rm ${DESTDIR}/${file} || true
	@test -L ${BUILDDIR}/${file} && unlink ${DESTDIR}/${file} || true

loop-check-package-lib:
	@echo "Checking libraries: ${file}"
	@${MAKE} loop=check-file loop-no \
		`ldd "${root}/${file}" 2>/dev/null| sed -n 's/.*=> \([^ ]*\) .*/${DESTDIR}\1/p' |grep -v ldd` 

loop-check-file:
	@echo librarie: ${file}
	@test -f ${DESTDIR}${file} || test -f ${root}/${file}
